# osVAC-Adapter

The osVAC project was startet in 2019 since we were tired of matching tools with different vacuum outlets.  
So we decided to setup one _standard open source adapter_: **the osVAC Adapter**!

>We started with 9 different adapters for different machines, vacuum hoses and cyclone connections and hope there will be more adapter for special purposes developed in the future by other others so all can benefit from a single universal adapter. And there are much more adapter and files which can be found at
https://www.thingiverse.com/hobbyhimmel/collections/osvac-adapter
and inside this repository.

More information about the project can be found here:
https://ossso.de/osvac/

## How does it work

Basically there is **one common bajonet based standard** defined. 
- The diameter is based on a 32mm standard hose. 
- As for each bajonet adapter there is a male and a female adapter. 
- **For osVAC the Female should always be on vacuum side.**
- In the meantime there are also some additional diameters and adapters. [Feel free to add your own](#contributing) 
  >But the amount of different Bajonet diameters should be kept at minimum!


![](https://gitlab.com/OSSSO/osVAC/osvac-adapter/-/raw/main/assets/example2.png)

---

## Repository structure

The repository is devided into 2 parts [**Adapters**](Adapter) and [**Templates**](Templates) (explanation below).

If you look for a specific Adapter file or in case you want to [contribute](#contribute) make sure to follow the [naming convention](#naming-convention).

### Naming convention

|Charater|optional| Description                            |Range          |
|:------:|:------:|----------------------------------------|---------------|
|  1     |        | Male/Female                            |[F,M]          |
|  2-3   |        | Adapter diameter (in mm)               |               |
|  4     |        | delimiter                              | -             |
|  5     |        | Type Hose, Round, Oval, Special, Flange|[H, R, O, S, F]|
|  6-7   |        | 2nd diamter (in mm)                    |               | 
|  8     |   x    | delimiter                              | -             |
|  9-... |   x    | additional information                 |               |

**Examples:**

-  `M32-H32`: osVAC Male adapter 32mm to hose with 32mm diameter
-  `M32-R53`: osVAC Male adapter 32mm to round 50mm
-  `F32-H32-wSlipRing`: osVAC Female adapter to hose with 32mm diameter with Slip Ring


### Adapters

**Here are ready to use/print adapter files stored.** 
- For each adapter at least an .stl Print File should be available, better a universal STEP file as well. (read only) 
- Prefered are .stl, .stp and the source files of the editor used during creation of the files. (editing)
- README File with Description, Creator contact info and optional Thingiverse Links (reference)
- 3D printing settings file (per printer) for stl printing options (optional)

Inside [**Adapter**](Adapter) most of the adpaters will be stored as standard sized M/F [Universal Adapter](Adapter/osVAC%20Universal%20Adapter). 
>But there is also a possibility to search for adapters by manufacturer and specific tools.


### Template Files

**Here are template source files stored to create new adapters.**
- Source files are available for different software. 
- There are specific README.md files explaining how to use the different files to create new adapters.

Inside [Templates](Templates) we prefere open formats such as [FreeCAD](Templates/FreeCAD).

---

## Contribute

This is a growing community driven Open Source (Hardware) project, with contributions we thrive. 

### Contributing
<!--  State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project. -->

In the current state of the projects you have 3 options to contribute:
- **GOLD** - [Fork](https://gitlab.com/OSSSO/osVAC/osvac-adapter/-/forks/new) this repository, add your changes/new files and send a [Merge Request](https://gitlab.com/OSSSO/osVAC/osvac-adapter/-/merge_requests/new)
- **SILVER** - Get someones [support](#support) from the community to work your way through GitLab Contributions
- **BRONZE** - Whatever request or new adapter addition is at your reach and you need a quick way - [submit an issue](https://gitlab.com/OSSSO/osVAC/osvac-adapter/-/issues/new)


### Support
- If you face any issue with existing adapter and files, feel free to [**create an issue**](https://gitlab.com/OSSSO/osVAC/osvac-adapter/-/issues/new) inside this repository. 
- You can also create issues if 
  - you are looking for adapters which are currently not available or 
  - you found an error that needs correction
  - you have any other request regarding the repository content
- contact and join via [telegram chat group](https://t.me/osvac_public) (German/English) 

---

## License
This is an Open Source Project licensed in its original form as [CC-BY-SA 4.0 International](LICENSE.md)

> Contact: 	HOBBYHIMMEL eMail: 	info@hobbyhimmel.de
>
> https://ossso.de/osvac/ (German)
