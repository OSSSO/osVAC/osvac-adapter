# osVAC Adapter

## osVAC Manufacturer
Inside this folder you can look for adapters based on manufacturer and tool type. Specific adapters will be stored directly inside this folder. For adapters which are not very special there can be only a link to the matching "osVAC Universal Adapter".
~~~
├── osVAC Manufacturer
│   ├── *Manufactorer Name*
│   │   └── *Tool Type*
~~~

## osVAC Universal Adapter
Inside this structure all universal adapters are stored, based on the following structure.
~~~
├── osVAC Universal Adapter
│   ├── F32
│   │   ├── Hose
│   │   ├── Round
│   │   └── Special
│   ├── Gates
│   └── M32
│       ├── Flange
│       ├── Hose
│       ├── Round
│       └── Special
~~~

## Other Adapter
Here can other Adapter without the osVAC bajonet be stored.

## Content
For each adapter at least a stl and step file should be available. If possible also the source files should be shared.
Details about the adapter and print settings should be stored inside the adapter README using specific keywords. Currently the following keywords can be used:
- Name
- Description
- Supports
- Resolution
- Infill


## Naming Convention

|Charater|optional| Description                            |Range          |
| :----: | :----: |----------------------------------------|---------------|
|  1     |        | Male/Female                            |[F,M]          |
|  2-3   |        | Adapter diameter (in mm)               |               |
|  4     |        | delimiter                              | -             |
|  5     |        | Type Hose, Round, Oval, Special, Flange|[H, R, O, S, F]|
|  6-7   |        | 2nd diamter (in mm)                    |               | 
|  8     |   x    | delimiter                              | -             |
|  9-... |   x    | additional information                 |               |

**Examples:**

-  M32-H32: osVAC Male adapter 32mm to hose with 32mm diameter
-  M32-R53: osVAC Male adapter 32mm to round 50mm
-  F32-H32-wSlipRing: osVAC Femaile adapter to hose with 32mm diameter with Slip Ring
