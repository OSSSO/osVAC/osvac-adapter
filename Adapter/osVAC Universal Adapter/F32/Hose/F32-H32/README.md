Contact: 	HOBBYHIMMEL 
eMail: 	info@hobbyhimmel.de
Thingiverse: 	https://www.thingiverse.com/thing:4562762

Summary:
This adapter is screwed on a DN32 hose and ends with the FEMALE part of our adapter system. It is also equipped with a slipring system (rotating part) so your hose won't get tangled up.
