Printer Settings:

Rafts:
Doesn't Matter

Supports:
No

Infill:
20

Notes:
Trouble in getting the adapter to turn?
in most slicers there setting like
Fill the gaps between the walls or fill the walls.
Make sure you have them set to the right paramter.
Also overextrusion might cause problems by "glueing" walls together.

If you have recommendations or details about the settings of your slicer software, please let us know and we add them in this text box. We want to learn from one another and succeed together :)
