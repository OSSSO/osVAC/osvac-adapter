Contact: 	HOBBYHIMMEL
eMail: 	info@hobbyhimmel.de
Thingiverse: 	https://www.thingiverse.com/thing:4563706

Summary:
This adapter is the standard adapter for most round outlets to our MALE adapter. The inner diameter is 40mm, the outer diameter is 45mm, inside and outside slightly tapered. For best permanent fixation to you tool or device you could use glue for example.
