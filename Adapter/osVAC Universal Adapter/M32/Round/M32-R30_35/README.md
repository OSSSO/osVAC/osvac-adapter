Contact: 	HOBBYHIMMEL
eMail: 	info@hobbyhimmel.de
Thingiverse: 	https://www.thingiverse.com/thing:4562808

Summary:
This adapter is the standard adapter for most round outlets to our MALE adapter. The inner diameter is 30mm, the outer diameter is 35mm, inside and outside slightly tapered. For best permanent fixation to you tool or device you could use glue for example.
